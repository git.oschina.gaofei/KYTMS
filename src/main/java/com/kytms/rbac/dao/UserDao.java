package com.kytms.rbac.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-18
 */
public interface UserDao<User> extends BaseDao<User> {
}
