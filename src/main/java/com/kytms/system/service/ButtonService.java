package com.kytms.system.service;

import com.kytms.core.model.CommModel;
import com.kytms.core.service.BaseService;

import java.util.List;
import java.util.Map;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-20
 */
public interface ButtonService<Button> extends BaseService<Button> {

    List<Button> selectButtonList(CommModel commModel);

    Map<String,String> selectUserButtonList(String userId);
}
